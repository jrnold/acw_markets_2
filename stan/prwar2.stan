data {
  int nobs;
  int n_times;
  int time[nobs];
  int n_series;
  int series[nobs];
  vector<lower=0.0>[nobs] logprice;
  int n_payments_max;
  int<lower=1, upper=n_payments_max> n_payments[nobs];
  vector[n_payments_max] payment[nobs];
  vector[n_payments_max] maturity[nobs];
  vector<lower=0.0>[nobs] yield_peace;
  vector<lower=0.0>[nobs] yield_war;
  real loglambda_init_mean;
  real<lower=0.0> loglambda_init_sd;
}
parameters {
  vector[n_times] loglambda_innov;
  real loglambda_init;
  real<lower=0.0> sigma;
  vector<lower=0.0>[n_series] psi;
}
transformed parameters {
  vector[nobs] mu;
  vector[n_times] loglambda;
  vector[n_times] lambda;
  loglambda[1] <- loglambda_init;
  for (i in 2:n_times) {
    loglambda[i] <- loglambda[i - 1] + sigma * loglambda_innov[i];
  }
  for (i in 1:n_times) {
    lambda[i] <- exp(loglambda[i]);
  }
  for (i in 1:nobs) {
    vector[n_payments[i]] discount;
    vector[n_payments[i]] payments_i;
    for (j in 1:n_payments[i]) {
      discount[j] <- (exp(- maturity[i, j] * yield_war[i])
                      - exp(- maturity[i, j] * (lambda[i] + yield_war[i]))
                      + exp(- maturity[i, j] * (lambda[i] + yield_peace[i])));
      payments_i[j] <- payment[i, j];
    }
    mu[i] <- log(dot_product(payments_i, discount));
  }
}
model {
  vector[nobs] psi_t;
  for (i in 1:nobs) {
    psi_t[i] <- psi[series[i]];
  }
  loglambda_init ~ normal(loglambda_init_mean, loglambda_init_sd);
  loglambda_innov ~ normal(0, 1);
  logprice ~ normal(mu, psi_t);
}
