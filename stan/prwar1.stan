data {
  int nobs;
  int n_times;
  int time[nobs];
  int n_series;
  int series[nobs];
  vector<lower=0.0>[nobs] logprice;
  int n_payments_max;
  int<lower=1, upper=n_payments_max> n_payments[nobs];
  vector[n_payments_max] payment[nobs];
  vector[n_payments_max] maturity[nobs];
  vector<lower=0.0>[nobs] yield_peace;
  vector<lower=0.0>[nobs] yield_war;
}
parameters {
  vector<lower=0.0>[n_times] lambda;
  vector<lower=0.0>[n_series] psi;
}
transformed parameters {
  vector[nobs] mu;
  for (i in 1:nobs) {
    vector[n_payments[i]] discount_war;
    vector[n_payments[i]] discount_peace;
    vector[n_payments[i]] payments_i;
    for (j in 1:n_payments[i]) {
      real log_pr_war;
      real log_pr_peace;
      log_pr_war <- exponential_cdf_log(maturity[i, j], lambda[time[i]]);
      log_pr_peace <- log1m_exp(log_pr_war);
      discount_war[j] <- exp(log_pr_war - maturity[i, j] * yield_war[i]);
      discount_peace[j] <- exp(log_pr_peace - maturity[i, j] * yield_peace[i]);
      payments_i[j] <- payment[i, j];
    }
    mu[i] <- log(dot_product(payments_i, discount_war + discount_peace));
  }
}
model {
  vector[nobs] psi_t;
  for (i in 1:nobs) {
    psi_t[i] <- psi[series[i]];
  }
  logprice ~ normal(mu, psi_t);
}
