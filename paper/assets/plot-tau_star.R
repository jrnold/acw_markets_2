source("conf.R")
fileargs <- commandArgs(TRUE)
fileout <- fileargs[1]

tau_star <- RDATA[["m_sixes_3a_summary"]][["tau_star"]]
colnames(tau_star) <- c("Confederate", "Inconclusive", "Union")
tau_star_summary <- adply(tau_star, 2, function(x) data.frame(x = x))

## gg <- (ggplot(tau_star_summary, aes(x = x, y = X1))
##        + geom_point(alpha = 1 / log(1024))
##        + scale_y_discrete("")
##        + scale_x_continuous("")
##        + theme_local())

gg <- (ggplot(tau_star_summary, aes(x = x, colour = X1))
       + geom_density()
       + scale_y_continuous("density")
       + scale_x_continuous("")
       + scale_colour_discrete("")
       + theme_local())

ggsave(gg, file = fileout, height = 2, width = 6.5)
