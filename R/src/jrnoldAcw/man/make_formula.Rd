\name{make_formula}
\alias{make_formula}
\title{Make additive formula}
\usage{
  make_formula(y, x.names, intercept = TRUE)
}
\arguments{
  \item{y}{\code{character} Name of outcome variable.}

  \item{x.names}{\code{character} Vector of covariate
  variable names.}

  \item{intercept}{\code{logical} Include an intercept in
  the formula.}
}
\description{
  Make a formula along the lines of \code{y ~ x1 + x2 + }.
}

