\name{ones}
\alias{ones}
\title{Ones vector}
\usage{
  ones(dim)
}
\arguments{
  \item{dim}{dimensions}
}
\description{
  Ones vector
}

