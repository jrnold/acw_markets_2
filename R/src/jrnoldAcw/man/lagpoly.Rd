\name{lagpoly}
\alias{lagpoly}
\title{Lag Polynomial}
\usage{
  lagpoly(x, k = 1, coef = 1, simplify = FALSE,
    na.pad = TRUE)
}
\arguments{
  \item{x}{\code{vector}}

  \item{k}{\code{integer}. Integer of lags and leads for
  columns. Positive integers are lags, negative integers
  are leads.}

  \item{coef}{\code{numeric} polynomial coefficients.}

  \item{simplify}{\code{logical}. Add the terms of the
  polynomial}

  \item{na.pad}{\code{logical}. Whether to drop all
  observations before the maximum lag or after the maximum
  lead.}
}
\value{
  Matrix with the a number of rows equal to the length of
  \code{x}, and columns for each lag in \code{k}.
}
\description{
  Creates a lag polynomial basis from a vector.
}

