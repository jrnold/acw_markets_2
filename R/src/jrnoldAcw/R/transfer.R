
##' Impulse response of a transfer function
##'
##' \eqn{
##' \mu_t = \sum \beta_{i=0}^{s} X_{t-i} + \sum_{i=1}^{r} \rho_{i} \mu_{t - i}
##' }
##'
##' @param x Numeric \code{matrix} of dimension (n, k)
##' @param T Max time
##' @param beta Numeric \code{matrix} of dimension (k, s)
##' @param rho \code{numeric} vector with length r.
##' @return Numeric \code{matrix} of dimension (n, T) with
##' the value of the impulse response function for each observation
##' (row) for times 1 to \code{T} (columns).
##' @export
impulse_response <- function(x, T=1, beta=0, rho=0) {
    if (is.null(dim(x))) {
        x <- t(matrix(x))
    }
    if (is.null(dim(beta))) {
        beta <- matrix(beta)
    }
    k <- min(T, ncol(beta))
    beta <- as.matrix(beta)[ , seq_len(k), drop=FALSE]

    n_rho <- length(rho)
    n_obs <- nrow(x)
    mu <- matrix(0, n_obs, T)
    mu[ , seq_len(k)] <- x %*% beta
    if (T > 1) {
        for (t in 2:T) {
            tmp <- rep(0, n_obs)
            for (j in seq_len(min(n_rho, t - 1))) {
                ell <- t - j
                tmp <- tmp + rho[j] * mu[ , ell]
            }
            mu[ , t] <- mu[ , t] + tmp
        }
    }
    mu
}

##' Step response of a transfer function
##'
##' @param ... passed to \code{\link{impulse_response}}
##' @export
step_response <- function(...) {
    t(apply(impulse_response(...), 1, cumsum))
}
