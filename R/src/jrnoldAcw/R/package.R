##' jrnoldAcw
##'
##' Code related to my American Civil War Markets project.
##'
##' @name jrnoldAcw
##' @docType package
##' @import stats
##' @import lubridate
##' @import plyr
##' @import reshape2
##' @import dlm
##' @import rstan
##' @import stringr
##' @import stanmisc
NULL
